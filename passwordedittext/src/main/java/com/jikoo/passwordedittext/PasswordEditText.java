package com.jikoo.passwordedittext;

import android.app.Activity;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Rect;
import android.support.v7.widget.AppCompatEditText;
import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.AttributeSet;
import android.util.DisplayMetrics;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.view.inputmethod.InputMethodManager;
import android.widget.CheckBox;
import android.widget.PopupWindow;

import java.util.regex.Pattern;

/**
 * Created by jyotishman.baruah on 18/06/18.
 */

public class PasswordEditText extends AppCompatEditText implements View.OnFocusChangeListener, TextWatcher {

    private int popupHeight;
    private boolean isPopupShown;
    private PopupWindow popUpWindow;
    private int screenHeight;
    private int screenWidth;
    private int popupWidth;
    private int xOffset;
    private int yOffset;


    private int numMinChars;
    private int numMaxChars;
    private int numNumeric;
    private int numLowerCase;
    private int numUpperCase;
    private int numSpecialChars;
    private String specialCharacters;
    private int style;

    private String specialCaseRegex;
    private String numRegex;
    private String lowercaseRegex;
    private String upperCaseRegex;
    private String lengthRegex;

    private boolean isSpecialCountValid;
    private boolean isNumericCountValid;
    private boolean isUpperCountValid;
    private boolean isLowerCountValid;
    private boolean isLengthValid;
    private ErrorViewHolder viewHolder;


    public PasswordEditText(Context context) {
        super(context);
        init(context,null);

    }

    public PasswordEditText(Context context, AttributeSet attrs) {
        super(context, attrs);

        init(context,attrs);

    }

    public PasswordEditText(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context,attrs);

    }


    @Override
    public void onFocusChange(View v, boolean hasFocus) {
        if (hasFocus)
            showPopup();
        else
            hidePopup();
    }

    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {
        if (s.length() == 0 )
            return;
        checkLength(s);
        checkLowercase(s);
        checkUpperCase(s);
        checkNumeric(s);
        checkSpecial(s);

        viewHolder.lengthCheckBox.setChecked(isLengthValid);
        viewHolder.lowerCaseCheckBox.setChecked(isLowerCountValid);
        viewHolder.upperCaseCheckBox.setChecked(isUpperCountValid);
        viewHolder.numericCheckBox.setChecked(isNumericCountValid);
        viewHolder.specialCheckBox.setChecked(isSpecialCountValid);
    }



    @Override
    public void afterTextChanged(Editable s) {

    }


    private void init(Context context, AttributeSet attrs) {

        setDefaultValidation();

        setAttrValidation(context,attrs);

        createValidationRegex();

        DisplayMetrics displayMetrics = new DisplayMetrics();
        ((Activity) getContext()).getWindowManager()
                .getDefaultDisplay()
                .getMetrics(displayMetrics);
        screenHeight = displayMetrics.heightPixels;
        screenWidth = displayMetrics.widthPixels;

        popupWidth =  3 * screenWidth / 4;

        final View popupView;
        if (style == -1) {
            popupView = LayoutInflater.from(getContext()).inflate(R.layout.layout_popup_card, null);
        }else{
            popupView = LayoutInflater.from(getContext()).cloneInContext(new ContextThemeWrapper(getContext(),style)).inflate(R.layout.layout_popup_card, null);

        }
        viewHolder = new ErrorViewHolder(popupView);

        setErrorViewTexts();

        popupView.getViewTreeObserver().addOnGlobalLayoutListener(new ViewTreeObserver.OnGlobalLayoutListener() {
            @Override
            public void onGlobalLayout() {
                popupHeight =  popupView.getHeight();
                popupView.getViewTreeObserver().removeOnGlobalLayoutListener(this);

                xOffset = (int) ((screenWidth-popupWidth)/2 - getX());
                yOffset = -getHeight() - popupHeight;
                popUpWindow.dismiss();
                popUpWindow.getContentView().setVisibility(VISIBLE);
                popUpWindow.showAsDropDown(PasswordEditText.this,0,0);

            }
        });


        popUpWindow  = new PopupWindow(popupView, popupWidth, ViewGroup.LayoutParams.WRAP_CONTENT);

        setOnFocusChangeListener(this);
        
        removeFocusOnTouchOutside();

        addTextChangedListener(this);


    }

    private void setErrorViewTexts() {
        viewHolder.lengthCheckBox.setText("Min > "+numMinChars+ " Max > "+numMaxChars);
        viewHolder.lowerCaseCheckBox.setText("Atleast "+numLowerCase+" lowercase{a-z}");
        viewHolder.upperCaseCheckBox.setText("Atleast "+numUpperCase+" uppercase{A-Z}");
        viewHolder.numericCheckBox.setText("Atleast "+numNumeric+" digits{0-9}");
        viewHolder.specialCheckBox.setText("Atleast "+numSpecialChars+" special chars{"+specialCharacters+"}");
    }

    private void setDefaultValidation() {
        numMaxChars = 14;
        numMinChars = 8;
        numSpecialChars =2;
        numUpperCase = 3;
        numLowerCase = 1;
        numNumeric = 2;
        specialCharacters = "!@#$%^&*";
    }

    private void setAttrValidation(Context context, AttributeSet attrs) {

        TypedArray ta = context.obtainStyledAttributes(attrs, R.styleable.PasswordEditText, 0, 0);
        try {
            numMaxChars = ta.getInteger(R.styleable.PasswordEditText_maxLength, numMaxChars);
            numMinChars = ta.getInteger(R.styleable.PasswordEditText_minLength, numMinChars);
            numSpecialChars = ta.getInteger(R.styleable.PasswordEditText_numSpecialCharacters, numSpecialChars);
            numUpperCase = ta.getInteger(R.styleable.PasswordEditText_numUpperCase, numUpperCase);
            numLowerCase = ta.getInteger(R.styleable.PasswordEditText_numLowerCase, numLowerCase);
            numNumeric = ta.getInteger(R.styleable.PasswordEditText_numDigits, numNumeric);
            specialCharacters = ta.getString(R.styleable.PasswordEditText_specialCharacters);
            if (TextUtils.isEmpty(specialCharacters))
                specialCharacters  =  "!@#$%^&*";
            style = ta.getResourceId(R.styleable.PasswordEditText_popupTheme, -1);

        } finally {
            ta.recycle();
        }
    }

    private void createValidationRegex() {
         lengthRegex = "^.{"+numMinChars+","+numMaxChars+"}$";
         numRegex = "(?=(.*[0-9]){"+numNumeric+"})^.*" ;
         lowercaseRegex = "(?=(.*[a-z]){"+numLowerCase+"})^.*" ;
         upperCaseRegex = "(?=(.*[A-Z]){"+numUpperCase+"})^.*" ;
         specialCaseRegex = "(?=(.*["+specialCharacters+"]){"+numSpecialChars+"})^.*" ;
    }

    private void checkSpecial(CharSequence s) {
        isSpecialCountValid =  s.toString().matches(specialCaseRegex);
    }

    private void checkNumeric(CharSequence s) {
        isNumericCountValid =  s.toString().matches(numRegex);

    }

    private void checkUpperCase(CharSequence s) {
        isUpperCountValid =  s.toString().matches(upperCaseRegex);

    }

    private void checkLowercase(CharSequence s) {
        isLowerCountValid =  s.toString().matches(lowercaseRegex);

    }

    private void checkLength(CharSequence s) {
        isLengthValid = Pattern.compile(lengthRegex).matcher(s.toString().trim()).matches();
    }


    private void removeFocusOnTouchOutside() {

        View touchInterceptor = ((Activity)getContext()).getWindow().getDecorView();
        touchInterceptor.setOnTouchListener(new OnTouchListener() {
            
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (event.getAction() == MotionEvent.ACTION_DOWN) {
                    if (PasswordEditText.this.isFocused()) {
                        Rect outRect = new Rect();
                        PasswordEditText.this.getGlobalVisibleRect(outRect);
                        if (!outRect.contains((int)event.getRawX(), (int)event.getRawY())) {
                            PasswordEditText.this.clearFocus();
                            InputMethodManager imm = (InputMethodManager) v.getContext().getSystemService(Context.INPUT_METHOD_SERVICE);
                            imm.hideSoftInputFromWindow(v.getWindowToken(), 0);
                        }
                    }
                }
                return false;
            }
        });
    }

    private void hidePopup() {
        if (!isPopupShown)
            return;
        popUpWindow.dismiss();
        isPopupShown  = false;

    }

    private void showPopup() {

        if (isPopupShown)
            return;

        if (popupHeight == 0) {
            popUpWindow.getContentView().setVisibility(INVISIBLE);
        }
        popUpWindow.showAsDropDown(PasswordEditText.this,xOffset,yOffset);

        isPopupShown = true;
    }


    private class ErrorViewHolder {

        CheckBox lengthCheckBox;
        CheckBox numericCheckBox;
        CheckBox lowerCaseCheckBox;
        CheckBox upperCaseCheckBox;
        CheckBox specialCheckBox;

        public ErrorViewHolder(View popupView) {
            lengthCheckBox = popupView.findViewById(R.id.cb_length);
            numericCheckBox = popupView.findViewById(R.id.cb_numeric);
            lowerCaseCheckBox = popupView.findViewById(R.id.cb_lower);
            upperCaseCheckBox = popupView.findViewById(R.id.cb_upper);
            specialCheckBox = popupView.findViewById(R.id.cb_special);

        }
    }
}
