package com.jikoo.dashededittext;

/**
 * Created by jyotishman.baruah on 13/06/18.
 */

interface EditCodeWatcher {
    void onCodeChanged(String code);
}
