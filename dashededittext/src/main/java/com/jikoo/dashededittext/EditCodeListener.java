package com.jikoo.dashededittext;

/**
 * Created by jyotishman.baruah on 13/06/18.
 */

interface EditCodeListener {

    void onCodeReady(String code);
}
