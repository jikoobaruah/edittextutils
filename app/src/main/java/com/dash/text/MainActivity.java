package com.dash.text;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import com.jikoo.mylibrary.DropCapTextView;

public class MainActivity extends AppCompatActivity {

    private DropCapTextView tv1, tv2;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        String html = "<b>Generations</b> <u>of</u> <b>Indians brought</b> up in a <i>shortage <u>economy</u></i> are living <b><i>testimonies</b> to the Shakespearean adage – <b>neither a borrower "
                + "nor a "
                + "lender "
                + "be</b></i>. "
                + "They have "
                + "been "
                + "reluctant "
                + "to "
                + "splurge "
                + "loaned money on what they THOUGHT were sheer <u>luxuries</u>: <u>Air-conditioners</u>, appliances, television sets or cooking ranges.<br/>"
                + "<br/>"
                + "These so-called modern conveniences are no longer status symbols in a rapidly urbanising India, and traditional lenders to big business are funding purchases of "
                + "these "
                + "items in "
                + "the hope of building a stable revenue stream anchored in durable consumption.<br/>"
                + "<br/>"
                + "To offset muted credit <a href=\"kdfhg\">demand</a> from companies, private lenders such as <a href=\"etandroidapp://company/9194\">ICICI Bank</a> and HDFC Bank are focusing on "
                + "<b><u><i>financing</b></u></i> consumer durables "
                + "that "
                + "has hitherto been "
                + "the "
                + "fiefdom of "
                + "NBFCs such as Bajaj Finance.";


        tv1 = findViewById(R.id.tv_1);
        tv2 = findViewById(R.id.tv_2);

        tv1.setText(html);
        tv2.setText(html);
    }
}
