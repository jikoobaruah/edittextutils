package com.jikoo.mylibrary;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Build;
import android.support.annotation.Nullable;
import android.support.annotation.RequiresApi;
import android.text.Html;
import android.text.Spanned;
import android.text.style.StyleSpan;
import android.text.style.URLSpan;
import android.text.style.UnderlineSpan;
import android.util.AttributeSet;
import android.util.Log;
import android.util.SparseArray;
import android.util.SparseBooleanArray;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Toast;

import java.util.HashMap;
import java.util.Map;

/**
 * Created by jyotishman.baruah on 25/06/18.
 */

public class DropCapTextView extends View {

//    private static final int paddingRight = 20;
//    private static final int paddingLeft = 20;
//    private static final int numDropCapLines = 3;
//    private static final int paddingBottom = 20;
//    private static final int paddingTop = 20;


    private static final int SPACING_CHAR = 2;
    private static final int SPACING_WORD = 10;
    private static final int SPACING_LINE = 10;
    private static final String TAG = "Jyo";

    //configurable 
    private Spanned text;
    private int paddingRight;
    private int paddingLeft;
    private int paddingTop;
    private int paddingBottom;
    private int numDropCapLines;
    private int textSize;


    private int charX;
    private int charY;
    private int dropCapWidth;
    private int currentLine;
    private Paint textPaint;
    private Typeface font;
    private SparseBooleanArray underlineSpans;
    private SparseBooleanArray boldSpans;
    private SparseBooleanArray italicSpans;
    private SparseArray<String> urlSpans;
    private Paint underLinePaint;
    private Rect underLineRect;
    private Paint urlPaint;

    private HashMap<Rect, String> urlRectMap;
    private int wrappedHeight;


    public DropCapTextView(Context context) {
        super(context);
        init();
    }

    public DropCapTextView(Context context, @Nullable AttributeSet attrs) {
        super(context, attrs);
        init();

    }

    public DropCapTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init();

    }

    @RequiresApi(api = Build.VERSION_CODES.LOLLIPOP)
    public DropCapTextView(Context context, @Nullable AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init();

    }

    public void setText(String htmlText) {
        text = Html.fromHtml(htmlText);

        scanSpans();

        invalidate();
    }

    private void scanSpans() {
        boldSpans = new SparseBooleanArray();
        italicSpans = new SparseBooleanArray();
        underlineSpans = new SparseBooleanArray();
        urlSpans = new SparseArray<>();


        StyleSpan[] styleSpans = text.getSpans(0, text.length(), StyleSpan.class);
        for (StyleSpan styleSpan : styleSpans) {
            for (int i = text.getSpanStart(styleSpan); i < text.getSpanEnd(styleSpan); i++) {
                if (styleSpan.getStyle() == Typeface.BOLD) {
                    boldSpans.put(i, true);
                }
                if (styleSpan.getStyle() == Typeface.ITALIC) {
                    italicSpans.put(i, true);
                }
                if (styleSpan.getStyle() == Typeface.BOLD_ITALIC) {
                    boldSpans.put(i, true);
                    italicSpans.put(i, true);
                }
            }
        }

        UnderlineSpan[] underlineSpans = text.getSpans(0, text.length(), UnderlineSpan.class);
        for (UnderlineSpan underlineSpan : underlineSpans) {
            for (int i = text.getSpanStart(underlineSpan); i < text.getSpanEnd(underlineSpan); i++) {
                this.underlineSpans.put(i, true);
            }
        }

        URLSpan[] urlSpans = text.getSpans(0, text.length(), URLSpan.class);
        for (URLSpan urlSpan : urlSpans) {
            for (int i = text.getSpanStart(urlSpan); i < text.getSpanEnd(urlSpan); i++) {
                this.urlSpans.put(i, urlSpan.getURL());
            }
        }

    }

    private void init() {

        loadDefaultConfigs();

        urlRectMap = new HashMap<>();

        textPaint = new Paint();

        underLinePaint = new Paint();
        underLinePaint.setColor(Color.BLACK);
        underLinePaint.setStyle(Paint.Style.FILL);

        urlPaint = new Paint();
        urlPaint.setColor(Color.BLUE);
        urlPaint.setStyle(Paint.Style.FILL);

        underLineRect = new Rect();

        setBackgroundColor(Color.TRANSPARENT);
    }

    private void loadDefaultConfigs() {

        paddingRight = (int) getResources().getDimension(R.dimen.paddingRight);
        paddingLeft = (int) getResources().getDimension(R.dimen.paddingLeft);
        paddingTop = (int) getResources().getDimension(R.dimen.paddingTop);
        paddingBottom = (int) getResources().getDimension(R.dimen.paddingBottom);
        numDropCapLines = getResources().getInteger(R.integer.numDropCapLines);
        textSize = (int) getResources().getDimension(R.dimen.textSize);
    }


    @Override
    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);

        currentLine = 1;

        StringBuilder word = new StringBuilder();
        int wordIndex = 1;

        int length = text.length();
        for (int i = 0; i < length; i++) {

            if (text.charAt(i) == '\n') {
                if (word.length() > 0) {
                    drawWord(canvas, word, wordIndex, i - word.length());
                    word = new StringBuilder();
                    wordIndex++;
                }

                drawNextLine();

            } else {
                if (text.charAt(i) == ' ') {
                    drawWord(canvas, word, wordIndex, i - word.length());
                    drawSpace();
                    word = new StringBuilder();
                    wordIndex++;
                } else {
                    word.append(text.charAt(i));
                }
            }
        }


        if (getMeasuredHeight() == wrappedHeight){
            getLayoutParams().height = charY +paddingBottom;
            requestLayout();
        }

    }

    private void drawWord(Canvas canvas, StringBuilder word, int wordIndex, int wordStartCharIndex) {
        if (wordIndex == 1) {
            drawDropCapText(canvas, word.charAt(0));
            if (word.length() == 1) {
                return;
            }
            word.deleteCharAt(0);
            drawNormalWord(canvas, word, wordStartCharIndex + 1);
        } else {
            drawNormalWord(canvas, word, wordStartCharIndex);
        }
    }

    private void drawNormalWord(Canvas canvas, StringBuilder s, int wordStartCharIndex) {

        textPaint.setColor(Color.TRANSPARENT);
        textPaint.setTextSize(textSize);


        font = Typeface.createFromAsset(getContext().getAssets(), "fonts/MerriweatherSans-Regular.ttf");

        if (boldSpans.get(wordStartCharIndex)) {
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/MerriweatherSans-Bold.ttf");
        }

        if (italicSpans.get(wordStartCharIndex)) {
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/MerriweatherSans-Italic.ttf");
        }

        if (boldSpans.get(wordStartCharIndex) && italicSpans.get(wordStartCharIndex) || urlSpans.get(wordStartCharIndex) != null) {
            font = Typeface.createFromAsset(getContext().getAssets(), "fonts/MerriweatherSans-BoldItalic.ttf");
        }

        textPaint.setTypeface(Typeface.create(font, Typeface.NORMAL));

        canvas.drawText(s.toString(), charX, charY, textPaint);

        Rect textBounds = new Rect();
        textPaint.getTextBounds(s.toString(), 0, s.length(), textBounds);

        int endX = charX + textBounds.right;
        if (endX > getWidth() - paddingRight) { // text is too long to fit in current line
            //move to next line
            drawNextLine();
        }

        // rewrite the word
        if (urlSpans.get(wordStartCharIndex) != null) {
            textPaint.setColor(Color.BLUE);
        } else {
            textPaint.setColor(Color.BLACK);
        }
        canvas.drawText(s.toString(), charX, charY, textPaint);

        textPaint.getTextBounds(s.toString(), 0, s.length(), textBounds);
        endX = charX + textBounds.right;

        if (underlineSpans.get(wordStartCharIndex)) {
            underLineRect.top = charY + 6;
            underLineRect.left = charX;
            underLineRect.bottom = underLineRect.top + 5;
            underLineRect.right = endX;
            canvas.drawRect(underLineRect, underLinePaint);
        }

        if (urlSpans.get(wordStartCharIndex) != null) {
            underLineRect.top = charY + 6;
            underLineRect.left = charX;
            underLineRect.bottom = underLineRect.top + 5;
            underLineRect.right = endX;
            canvas.drawRect(underLineRect, urlPaint);
            Rect urlRect = new Rect();
            urlRect.left = charX;
            urlRect.right = endX;
            urlRect.top = charY - textSize;
            urlRect.bottom = charY;
            urlRectMap.put(urlRect, urlSpans.get(wordStartCharIndex));
        }

        charX = endX + SPACING_WORD;
        if (charX > getWidth() - paddingRight) {
            drawNextLine();
        }

    }

    private void drawDropCapText(Canvas canvas, char c) {

        charX = paddingLeft;
        charY = numDropCapLines * textSize + (numDropCapLines - 1) * SPACING_LINE + paddingTop;

        font = Typeface.createFromAsset(getContext().getAssets(), "fonts/MerriweatherSans-Bold.ttf");
        textPaint.setTypeface(Typeface.create(font, Typeface.NORMAL));
        textPaint.setColor(Color.RED);
        textPaint.setTextSize(numDropCapLines * textSize + (numDropCapLines - 1) * SPACING_LINE + paddingTop);

        canvas.drawText(Character.toString(c), charX, charY, textPaint);

        Rect textBounds = new Rect();
        textPaint.getTextBounds(Character.toString(c), 0, 1, textBounds);

        dropCapWidth = textBounds.right;

        charX = charX + textBounds.right + SPACING_CHAR;
        charY = textSize + paddingTop;

    }


    private void drawNextLine() {

        if (currentLine < numDropCapLines) {
            charX = dropCapWidth + paddingLeft + (currentLine == 1 ? SPACING_CHAR : SPACING_WORD);
        } else {
            charX = paddingLeft;
        }
        charY = charY + textSize + SPACING_LINE;

        currentLine++;

    }

    private void drawSpace() {
        if (currentLine <= numDropCapLines && charX == dropCapWidth + paddingLeft) {
            return;
        }
        if (currentLine > numDropCapLines && charX == paddingLeft) {
            return;
        }
        charX = charX + SPACING_WORD;
        if (charX > getWidth() - paddingRight) {
            drawNextLine();
        }
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
        if (event.getAction() == MotionEvent.ACTION_DOWN) {
            int x = (int) event.getX();
            int y = (int) event.getY();

            Rect rect;
            for (Map.Entry<Rect, String> entry : urlRectMap.entrySet()) {
                rect = entry.getKey();
                if (rect.left < x && rect.right > x && rect.top < y && rect.bottom > y) {
                    Toast.makeText(getContext(), entry.getValue(), Toast.LENGTH_LONG).show();
                    return true;
                }
            }

            return super.onTouchEvent(event);

        } else {
            return super.onTouchEvent(event);
        }
    }


        @Override
    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {

        int desiredWidth = getSuggestedMinimumWidth() + getPaddingLeft() + getPaddingRight();
        wrappedHeight = 20 + getPaddingTop() + getPaddingBottom();

        setMeasuredDimension(measureDimension(desiredWidth, widthMeasureSpec),
                measureDimension(wrappedHeight, heightMeasureSpec));
    }

    private int measureDimension(int desiredSize, int measureSpec) {
        int result;
        int specMode = MeasureSpec.getMode(measureSpec);
        int specSize = MeasureSpec.getSize(measureSpec);

        if (specMode == MeasureSpec.EXACTLY) {
            result = specSize;
        } else {
            result = desiredSize;
            if (specMode == MeasureSpec.AT_MOST) {
                result = Math.min(result, specSize);
            }
        }

        if (result < desiredSize){
            Log.e("ChartView", "The view is too small, the content might get cut");
        }
        return result;
    }
}
